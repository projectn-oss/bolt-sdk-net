# .NET SDK for Bolt

This .NET solution provides a .NET Client Library for Bolt.

It can be built using any of the standard .NET IDEs (including Microsoft Visual Studio and Jetbrains Rider) using the included project files.

## Using .NET SDK for Bolt

* Install the SDK package into your project using dotnet CLI:
   ```bash
   dotnet add package ProjectN.Bolt --version 1.0.0
   ```